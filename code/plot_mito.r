install.packages("circlize")
library(circlize)
set.seed(123)
df_bed = read.delim2("mt_svs.bed")

h <- as.numeric(as.character(df_bed$heteroplasmy))
df_bed$heteroplasmy <- ifelse(h > 5, h, 5)

df_bed$color <- "black"
df_bed$color <- ifelse((df_bed$type == "DEL") | (df_bed$type == "Deletion"), "#4682B480", df_bed$color)
df_bed$color <- ifelse((df_bed$type == "TD") | (df_bed$type == "Tandem duplications"), "#B2225380", df_bed$color)
df_bed$color <- ifelse((df_bed$type == "INV") | (df_bed$type == "Inversions"), "#00800080", df_bed$color)
df_bed$color <- ifelse((df_bed$type == "INS") | (df_bed$type == "Insertion"), "#FFA50080", df_bed$color)
df_bed$chr = "chrM"

thickness_adj <- 1

df_bed1 <- df_bed[,c("chr", "start", "heteroplasmy", "color")]
df_bed1$end <- df_bed1$start
df_bed1$start <- as.integer(df_bed1$start - (df_bed1$heteroplasmy *thickness_adj))
df_bed1 <- df_bed1[,c("chr", "start", "end", "heteroplasmy", "color")]

df_bed2 <- df_bed[,c("chr", "end", "heteroplasmy", "color")]
df_bed2$start <- df_bed2$end
df_bed2$end <- as.integer(df_bed2$end + (df_bed2$heteroplasmy * thickness_adj))
df_bed2 <- df_bed2[,c("chr", "start", "end", "heteroplasmy", "color")]

#############
add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2, 
                     function(x) 
                       rgb(x[1], x[2], x[3], alpha=alpha))  
}
## SNVs
df_snv_bed <- read.delim2("mt_snv.bed")
df_snv_bed$heteroplasmy <- as.numeric(as.character(df_snv_bed$heteroplasmy))
df_snv_bed$snv_color <- as.factor(add.alpha("firebrick", df_snv_bed$heteroplasmy))

df_snv_bed <- df_snv_bed[,c("chr", "start", "heteroplasmy","Gene", "snv_color")]
head(df_snv_bed)


#### genes ####
df_mitogenes <- read.delim("./data/mt_dna_mm10_gene.txt")
df_mitogenes$chr <- "chrM"
df_mitogenes$start <- as.numeric(df_mitogenes$start)
df_mitogenes$end <- as.numeric(df_mitogenes$end)
table(df_mitogenes$type)
df_mitogenes2 <- df_mitogenes[df_mitogenes$type != "TSS region", c("chr", "start", "end", "name", "type")]


#### regulation ####
df_mitoreg <- read.delim("./data/mtDNA_regulation_mouse.txt")
df_mitoreg$start <- as.numeric(df_mitoreg$start)
df_mitoreg$end <- as.numeric(df_mitoreg$end)
table(df_mitoreg$type)
df_mitoreg2 <- df_mitoreg[, c("chr", "start", "end", "name", "type")]


#pdf("/Users/wlp842/Dropbox (WeischenfeldtLab)/papers/Papers_coAuthor/2018_Elham_mtDNA/figures/mtDNA_SVS_circos.pdf")
pdf("mtDNA_SV_SNV_circos.pdf")
circos.clear()
circos.par(start.degree = 90)
circos.initializeWithIdeogram(species = "mm10", chromosome.index = "chrM")

## intrinsic features
for (i in 1:nrow(df_mitoreg2)){
  df_mitoreg3 <- df_mitoreg2[i,]
  name <- as.character(df_mitoreg3$name)
  first <- round(df_mitoreg3$start/16299 * 360)
  last <- round(df_mitoreg3$end/16299 *360)
  center = df_mitoreg3$start + (df_mitoreg3$end-df_mitoreg3$start)/2
  cat (name, first, last, center,  "\n")
draw.sector(start.degree = -first+90, end.degree = -last+90, rou1 = 0.87, rou2 = 0.85, col = "#4682B480", clock.wise = T, border = TRUE)
 circos.text(center, -0.9, name, cex = 0.6)
}

circos.genomicLabels(df_mitogenes2, labels.column = 4, side = "inner",
    #col = as.numeric(factor(df_mitogenes2[[5]])), line_col = as.numeric(factor(df_mitogenes2[[5]])))
    col = rep("black", nrow(df_mitogenes2)), line_col = rep("grey", nrow(df_mitogenes2)),
    labels_height = 0.2, cex = 0.4)


## SNVs
for (i in 1:nrow(df_snv_bed)){
  df_snv_bed_i <- df_snv_bed[i,]
  name <- as.character(df_snv_bed_i$Gene)
  first <- round(df_snv_bed_i$start/16299 * 360)
  last <- round((df_snv_bed_i$start + 1)/16299 *360)
  center = df_snv_bed_i$start
  color = as.vector(df_snv_bed_i$snv_color)
  cat (name, first, last, center,  "\n")
draw.sector(start.degree = -first+90, end.degree = -last+90, rou1 = 0.58, rou2 = 0.6, col = color, clock.wise = T, 
            #border = F, 
            lwd = 1, border = color)
circos.text(center, -0.4, name, cex = 0.4, facing = "downward")
}


circos.genomicLink(df_bed1, df_bed2, 
                   col = df_bed1[[5]], 
    border = NA)

dev.off()