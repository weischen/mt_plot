#!/usr/bin/env python
## created: 190811
## by: Joachim Weischenfeldt
## joachim.weischenfeldt@gmail.com
from __future__ import division, print_function
import pandas as pd 
import numpy as np
import re

# SVs
df_sv = pd.read_excel("SVs.xlsx", index_col=[0])
df_sv.reset_index(inplace = True)
df_sv["start"] = df_sv["Breakpoints"].str.split("-",expand = True)[0].astype(int)
df_sv["end"] = df_sv["Breakpoints"].str.split("-",expand = True)[1].astype(int)
df_sv["end"] = np.where(df_sv["start"] == df_sv["end"], df_sv["end"] + 1, df_sv["end"])
df_sv["chr"] = "MT"
df_sv["heteroplasmy"] = df_sv["Heteroplasmy%"].astype(int).apply(lambda x: min(100, x))
df_sv["type"] = df_sv["Type of SVs"]
df_sv[["chr", "start", "end", "type", "heteroplasmy"]].to_csv("mt_svs.bed", index = False, sep = "\t")

# SNVs
df_snv = pd.read_excel("SNVs.xlsx")
df_snv["chr"] = "MT"
df_snv["start"] = df_snv["Substitution"].apply(lambda x: re.sub(r"m.([0-9]*).*", r"\1", x)).astype(int)
df_snv["heteroplasmy"] = df_snv["Heteroplasmy%"].astype("str").str.replace("%", "").astype(float)/100
df_snv[["chr", "start", "heteroplasmy", "Gene"]].to_csv("mt_snv.bed", index = False, sep = "\t")
