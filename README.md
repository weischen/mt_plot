# plot SVs and SNVs on mitochondrial genome
input SNV and SV list
Using unstructed format from Shoreh group as input

### Requirements:

R package `circlize` and R version 3.2 or higher

python packages: `pandas` `numpy` and python 2.7 or higher

## Execution

### 1. copy two files to the current directory:

SVs.xlsx

SNVs.xlsx

## 2. run the following command:

`bash mito_plot.sh`



